## Lancer QoL

This module has become a grab-bag of anything I can code that
helps me run my Lancer games in Foundry VTT. This includes automating
wreck images, some condition effects, reaction reminders, etc.

### Condition Effects

![example gif](images/example.gif)

- Adds an orange glow effect to tokens when in the `Danger Zone`.
- Adds a blue glow effect to tokens when they have `Overshield`.
- Adds a flame effect to tokens when they have `Burn`.
- Adds an electric field effect to tokens that are `Jammed`.
- Adds a ghostly effect to tokens that are `Invisible`.
- Wreck - Applies a random wreck image to a token.

### Reaction Reminder

When a token that you own is targeted and has reaction systems,
have a pop-up window or chat message to remind you to use them.

![reaction reminder chat message](images/reactionReminder.png)

### Effects Timer :warning:

This is a beta feature to try and have some automation to remove
conditions from tokens in combat after a set number of turns and
rounds.

### Macros

This will add a macro compendium to your game.
- Scan to Journal - This scans a token and creates a journal entry for
  player reference.
- HP0 - A macro that is aware of scratched paint the can replace the
  built in Lancer system automation for structure.
- Update Ammo Cases - A macro that will try to update all mechs with
  proper Ammo Case systems based on their current pilot talents.

Note: I've recently removed some macros that I am no longer maintaining
since the module does what those macros were used for.

## Install

You can install this module from within the Foundry VTT module list.

Or use this link to install the module.

```
https://gitlab.com/csmcfarland/csm-lancer-qol/-/raw/main/module.json
```

## Having Trouble?

[Troubleshooting Guide][trouble]

## Configuration

### Enable Automation

By default, the automations are enabled. But you can turn them off.

### Enable Condition Effects

This will enable or disable the token magic effects that are applied
based on certain conditions.

### Enable Wrecks Automation

You can turn this off if you don't want to use wrecks.

### Wreck Type

If wrecks are enabled, this controls whether they will be tokens (remaining targetable and selectable), or tiles (no longer selectable/targetable, so good for reducing UI clutter).

Four options are available:
* All wrecks are tokens
* All wrecks are tiles
* Player mechs wreck as tokens, NPCs wreck as tiles
* Linked tokens wrecks as tokens, unlinked tokens wreck as tiles

Tile wrecks can be restored to tokens (with 1hp and 1 stucture) via a button in the tile HUD (right click the tile, while in the tile tool).

### Root Wrecks Folder

Setting this folder allows you to add your own wrecks to the random images
selected when a mech has 0 Structure, 0 HP, and wreck automation is enabled.

This is a folder that contains images to use as wreck tokens. This folder
should be in the `User Data`. Inside the folder, you will need to have
folders called `s1`, `s2`, `s3` that correspond to the images inside that
are for that size token. The file names do not matter inside the size
folders.

For example, if you set this to`wrecks`, it would need a folder structure
inside that folder like this:

```
wrecks
├─s1
├─s2
├─s3
```

## Reaction Reminder

Disabled by default, select a pop-up message or a chat message for the
reminder.

## Effects Timer

Disabled by default as it's a new feature, this attempts to allow setting
conditions on combat tokens and having them removed automatically.

1. Enable the feature in the settings
2. Drag out the macro `Timed Effect` from the `Lancer CSM` compendium
3. Use the macro to assign conditions to a selected token

![effect timer macro dialog](images/effectimer.png)

When setting these values, just keep in mind that this does not support
conditions ending on an `originator` token turn (yet!).

I hope to improve on this interface, but for now, if you have a condition
like `impaired` that lasts until the end of your next turn, you would:

1. Select the token and run the macro in combat
2. Select the `Impaired` condition and set the duration to `end of targets next turn`
3. Add a note for what caused the condition
4. Hope it works!

### Debug Mode

Enable this to get more verbose debug logs in the browser console.

## Thank You and Attributions

The Wreck images included in this module were graciously allowed to be
included from [Retrograde Minis][rgm]. If you want to add more wrecks,
or are looking for minis, I highly recommend them.

- [Retrograde Minis Patreon][rgmp]

A lot of this code started out as things I copied from the #lancer-vtt
discord channel. This module was made while standing on the shoulders of
giants. Do not hesitate to contact me to add your name here, or remove code
that you wrote, or if you want to help or add more.

Thank You!

- Eranziel
- Valkyrion
- dodgepong
- That Lancer Clocks Guy
- Zenn
- Cake
- Freeze

[rgm]: https://www.retrogrademinis.com/
[rgmp]: https://www.patreon.com/RetrogradeMinis
[trouble]: https://gitlab.com/csmcfarland/csm-lancer-qol/-/wikis/Troubleshooting-Lancer-QoL
