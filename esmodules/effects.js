// Foundry VTT Based Active Effects
export const qolStatusEffects = [
    {
        id: "burn",
        label: "Burn",
        icon: "icons/svg/fire.svg"
    },
    {
        id: "overshield",
        label: "Overshield",
        icon: "icons/svg/shield.svg"
    }
]

// TokenMagic FX Definitions
export const dangerZoneEffect = [
    {
        filterType: "glow",
        filterId: "DangerZoneGlow",
        outerStrength: 4,
        innerStrength: 2,
        color: 0xff9633,
        quality: 0.5,
        padding: 10,
        animated: {
            color: {
                active: true,
                loopDuration: 6000,
                animType: "colorOscillation",
                val1: 0xEE5500,
                val2: 0xff9633
            },
            outerStrength: {
                active: true,
                loopDuration: 6000,
                animType: "cosOscillation",
                val1: 2,
                val2: 5
            }
        }
    },
    {
        filterType: "xbloom",
        filterId: "DangerZoneBloom",
        threshold: 0.35,
        bloomScale: 0,
        brightness: 1,
        blur: 0.1,
        padding: 10,
        quality: 15,
        blendMode: 0,
        animated: {
            bloomScale: {
                active: true,
                loopDuration: 6000,
                animType: "sinOscillation",
                val1: 0.4,
                val2: 1.0
            }
        }
    }
];
export const burnEffect = [
    {
        filterType: "xglow",
        filterId: "BurnGlow",
        auraType: 2,
        color: 0x903010,
        thickness: 9.8,
        scale: 4.,
        time: 0,
        auraIntensity: 2,
        subAuraIntensity: 1.5,
        threshold: 0.40,
        discard: true,
        animated: {
            time: {
                active: true,
                speed: 0.0027,
                animType: "move"
            },
            thickness: {
                active: true,
                loopDuration: 3000,
                animType: "cosOscillation",
                val1: 2,
                val2: 5
            }
        }
    }
];
export const overshieldEffect = [
    {
        filterType: "outline",
        filterId: "OverShieldGlow",
        padding: 10,
        color: 0x48dee0,
        thickness: 1,
        quality: 5,
        zOrder: 9,
        animated: {
            thickness: {
                active: true,
                loopDuration: 800,
                animType: "syncCosOscillation",
                val1: 1,
                val2: 6
            }
        }
    }
];
export const invisibleEffect = [
    {
        filterType: "liquid",
        filterId: "invisible",
        color: 0x20AAEE,
        time: 0,
        blend: 8,
        intensity: 4,
        spectral: true,
        scale: 0.9,
        animated:
        {
            time:
            {
                active: true,
                speed: 0.0010,
                animType: "move"
            },
            color:
            {
                active: true,
                loopDuration: 6000,
                animType: "colorOscillation",
                val1: 0xFFFFFF,
                val2: 0x00AAFF
            }
        }
    }
];
export const jammedEffect = [
    {
        filterType: "electric",
        filterId: "jammed",
        color: 0xFFFFFF,
        time: 0,
        blend: 1,
        intensity: 5,
        animated:
        {
            time:
            {
                active: true,
                speed: 0.0020,
                animType: "move"
            }
        }
    }
];
