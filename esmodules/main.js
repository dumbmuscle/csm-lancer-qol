/*global CONFIG, TokenMagic, canvas, game, FilePicker, Hooks */

import { displayReactions } from "./reaction.js";
import { preWreck, canvasReadyWreck, preLoadImageForAll, isBiological, wreckIt, unWreckIt, unWreckTile } from "./wreck.js";
import { qolStatusEffects, dangerZoneEffect, burnEffect, overshieldEffect, invisibleEffect, jammedEffect } from "./effects.js";
import { log } from "./log.js";
import { combatTracking, setTimedEffect } from "./combatTracking.js";

function registerSettings() {
    game.settings.register('csm-lancer-qol', 'enableAutomation', {
        name: 'Enable Automation',
        hint: 'When set to true, automate Danger Zone, Burn, and Overshield Effects!',
        scope: 'world',
        config: true,
        type: Boolean,
        default: true,
        onChange: value => {
            log(`Setting enableAutomation to: ${value}`);
        },
    });
    game.settings.register('csm-lancer-qol', 'enableConditionEffects', {
        name: 'Enable Condition Effects',
        hint: 'When set to true, automate effects when conditions are applied!',
        scope: 'world',
        config: true,
        type: Boolean,
        default: true,
        onChange: value => {
            log(`Setting enableConditionEffects to: ${value}`);
        },
    });
    game.settings.register('csm-lancer-qol', 'enableAutomationWrecks', {
        name: 'Enable Wrecks Automation',
        hint: 'When set to true, automate wrecking things!',
        scope: 'world',
        config: true,
        type: Boolean,
        default: true,
        onChange: value => {
            log(`Setting enableAutomationWrecks to: ${value}`);
        },
    });
    game.settings.register('csm-lancer-qol', 'wreckType', {
        name: 'Wreck Type',
        hint: 'Select how to present wrecks',
        scope: 'world',
        config: true,
        type: String,
        choices: {
            'token' : 'All wrecks will be tokens (selectable, targetable)',
            'tile' : 'All wrecks will be tiles (not targetable)',
            'PCtoken' : 'Wrecks of Player Mechs will be tokens, NPCs will be tiles',
            'linkToken' : 'Wrecks of linked tokens will be tokens, others will be tiles'
        },
        default: true,
        onChange: value => {
            log(`Setting wreckType to: ${value}`);
        },
    });
    game.settings.register('csm-lancer-qol', 'userWrecksPath', {
        name: 'Wrecks Folder',
        hint: 'Location of your wrecks token images. Set to empty to disable.',
        scope: 'world',
        config: true,
        type: String,
        default: '',
        filePicker: 'folder',
        onChange: value => {
            log(`Setting userWrecksPath to: ${value}`);
        },
    });
    game.settings.register('csm-lancer-qol', 'reactionReminder', {
        name: 'Reaction Reminder',
        hint: 'Remind users they have reactions when a token is targeted.',
        scope: 'world',
        config: true,
        type: String,
        choices: {
            "d": "Disable",
            "c": "Chat Whisper",
            "p": "Pop-Up"
        },
        default: "d",
        onChange: value => {
            log(`Setting reactionReminder to: ${value}`);
        },
    });
    game.settings.register('csm-lancer-qol', 'effectsTimer', {
        name: 'Effects Timer',
        hint: '!!BETA!! Allow removing effects automatically during combat.',
        scope: 'world',
        config: true,
        type: Boolean,
        default: false,
        onChange: value => {
            log(`Setting effectsTimer to: ${value}`);
        },
    });
    game.settings.register('csm-lancer-qol', 'debug', {
        name: 'Debug Mode',
        hint: 'Enable more verbose console logs.',
        scope: 'world',
        config: true,
        type: Boolean,
        default: false,
        onChange: value => {
            log(`Setting debug to: ${value}`);
        },
    });
}

function addStatusEffects() {
    let statusEffects = [];
    console.log('addStatusEffects');
    statusEffects = statusEffects.concat(CONFIG.statusEffects);
    statusEffects = statusEffects.concat(qolStatusEffects);
    CONFIG.statusEffects = statusEffects;
}

async function updateStructure(token) {
    let structure = 0;
    let response = '';
    structure = game.version < 10 ?
        token.actor.data.data.structure :
        token.actor.system.structure;
    if (structure <= 0 && !isBiological(token)) {
        response = `${token.name} is a wreck!`;
        token = await wreckIt(token);
        const updates = game.version < 10 ?
            { data: { overshield: 0, burn: 0, heat: 0 } } :
            { system: { overshield: 0, burn: 0, heat: 0 } };
        if(token) await token.actor.update(updates);
    } else {
        response = `${token.name} is NOT a wreck.`;
        await unWreckIt(token);
    }
    return response;
}

async function updateHeat(actor) {
    let danger = 0;
    let response = '';
    const effect = {
        changes: [],
        duration: {
            startTime: 1,
            seconds: 1
        },
        icon: "systems/lancer/assets/icons/white/status_dangerzone.svg",
        label: "Danger Zone",
        flags: {
            'core': {
                statusEffectsEnabled: true,
                statusId: 'dangerzone'
            }
        }
    };
    danger = game.version < 10 ? actor.data.data.heat / actor.data.data.derived.heat.max : actor.system.heat / actor.system.derived.heat.max;
    log(danger);
    if (danger >= 0.5) {
        response = `${actor.name} is in the Danger Zone!`;
        if (actor.data.effects.filter(i => i.data.label === 'Danger Zone').length == 0) {
            await actor.createEmbeddedDocuments("ActiveEffect", [effect]);
        }
    } else {
        response = `${actor.name} is NOT in the Danger Zone!`;
        if (actor.data.effects.filter(i => i.data.label === 'Danger Zone').length > 0) {
            await actor.deleteEmbeddedDocuments("ActiveEffect", [actor.data.effects.find(i => i.data.label === 'Danger Zone').data._id]);
        }
    }
    return response;
}

async function updateBurn(actor) {
    let burn = 0;
    let response = '';
    const effect = {
        changes: [],
        duration: {
            startTime: 1,
            seconds: 1
        },
        icon: "icons/svg/fire.svg",
        label: "Burn",
        flags: {
            'core': {
                statusEffectsEnabled: true,
                statusId: 'burn'
            },
        }
    };
    burn = game.version < 10 ? actor.data.data.burn : actor.system.burn;
    log(burn);
    if (burn > 0) {
        response = `${actor.name} is burning!`;
        if (actor.data.effects.filter(i => i.data.label === 'Burn').length == 0) {
            await actor.createEmbeddedDocuments("ActiveEffect", [effect]);
        }
    } else {
        response = `${actor.name} is NOT burning.`;
        if (actor.data.effects.filter(i => i.data.label === 'Burn').length > 0) {
            await actor.deleteEmbeddedDocuments("ActiveEffect", [actor.data.effects.find(i => i.data.label === 'Burn').data._id]);
        }
    }
    return response;
}

async function updateOverShield(actor) {
    let shield = 0;
    let response = '';
    const effect = {
        changes: [],
        duration: {
            startTime: 1,
            seconds: 1
        },
        icon: "icons/svg/shield.svg",
        label: "Overshield",
        flags: {
            'core': {
                statusEffectsEnabled: true,
                statusId: 'overshield'
            }
        }
    };
    shield = game.version < 10 ? actor.data.data.overshield : actor.system.overshield;
    log(shield);
    if (shield > 0) {
        response = `${actor.name} is shielded!`;
        if (actor.data.effects.filter(i => i.data.label === 'Overshield').length == 0) {
            await actor.createEmbeddedDocuments("ActiveEffect", [effect]);
        }
    } else {
        response = `${actor.name} is NOT shielded.`;
        if (actor.data.effects.filter(i => i.data.label === 'Overshield').length > 0) {
            await actor.deleteEmbeddedDocuments("ActiveEffect", [actor.data.effects.find(i => i.data.label === 'Overshield').data._id]);
        }
    }
    return response;
}

async function updateEffect(label, token, enable) {
    const effectMap = [
        {
            label: 'Invisible',
            preset: invisibleEffect,
            effects: [
                'invisible'
            ]
        },
        {
            label: 'Jammed',
            preset: jammedEffect,
            effects: [
                'jammed'
            ]
        },
        {
            label: "Danger Zone",
            preset: dangerZoneEffect,
            effects: [
                'DangerZoneGlow',
                'DangerZoneBloom'
            ]
        },
        {
            label: "Burn",
            preset: burnEffect,
            effects: [
                'BurnGlow'
            ]
        },
        {
            label: "Overshield",
            preset: overshieldEffect,
            effects: [
                'OverShieldGlow'
            ]
        }
    ];
    // If I weren't a caveman, I would find a way to just search the effects.
    const effectData = effectMap.find(x => x.label === label);
    log(effectData);
    if (typeof effectData !== 'undefined') {
        if (enable) {
            if (!TokenMagic.hasFilterId(token, effectData.effects[0])) { await token.TMFXaddUpdateFilters(effectData.preset); }
        } else {
            for (var i = 0; i < effectData.effects.length; i++) {
                if (TokenMagic.hasFilterId(token, effectData.effects[i])) { await token.TMFXdeleteFilters(effectData.effects[i]); }
            }
        }
    }
    return label;
}

function findToken(actor) {
    if (actor.parent !== null) { // This actor is probably not linked
        var foundToken = game.version < 10 ?
            canvas.tokens.placeables.find(i => i.data._id === actor.parent.data._id) :
            canvas.tokens.placeables.find(i => i.document._id === actor.parent._id);
    } else { // This actor is probably linked
        var foundToken = game.version < 10 ?
            canvas.tokens.placeables.find(i => i.data.actorId === actor.data._id) :
            canvas.tokens.placeables.find(i => i.document.actorId === actor._id);
    }
    return foundToken;
}

function tileHUDButton(app, html, context) {
    const tile = app?.object?.document;
    if(!tile || !tile.getFlag('csm-lancer-qol', 'isWreck')) return //if there's no tile, or that tile is not a wreck, not our problem
    const button = $(`<div class="control-icon csm-lancer-qol" title="UnWreck"  data-tooltip="UnWreck"><i class="fas fa-person-rays"></i></div>`);

    button.on('mouseup', () => { unWreckTile(tile) });

    const column = '.col.right';
    html.find(column).append(button);
}

Hooks.on('updateActor', async function (document, change, options, userId) {
    if (game.settings.get('csm-lancer-qol', 'enableAutomation')) {
        log('**actorUpdate**');
        log(document);
        log(document.constructor.name);
        log(change);
        log(options);
        log(`${game.users.find(x => x.id === userId).name}(${userId})`);
        let foundToken = findToken(document);
        if (foundToken && game.userId === userId) { // Only if we find a valid token and we can most likely edit it ...

            if (game.version < 10) {

                if (typeof change.data?.structure !== 'undefined' && game.settings.get('csm-lancer-qol', 'enableAutomationWrecks')) {
                    log(await updateStructure(foundToken));
                }

                if (typeof change.data?.heat !== 'undefined') {
                    log(await updateHeat(document));
                }

                if (typeof change.data?.overshield !== 'undefined') {
                    log(await updateOverShield(document));
                }

                if (typeof change.data?.burn !== 'undefined') {
                    log(await updateBurn(document));
                }

            } else {

                if (typeof change.system?.structure !== 'undefined' && game.settings.get('csm-lancer-qol', 'enableAutomationWrecks')) {
                    log(await updateStructure(foundToken));
                }

                if (typeof change.system?.heat !== 'undefined') {
                    log(await updateHeat(document));
                }

                if (typeof change.system?.overshield !== 'undefined') {
                    log(await updateOverShield(document));
                }

                if (typeof change.system?.burn !== 'undefined') {
                    log(await updateBurn(document));
                }
            }
        }
    }
});

Hooks.on('createActiveEffect', async function (document, change, userId) {
    log('**createActiveEffect**');
    const label = game.version < 10 ?
        document.data.label :
        document.label;
    log(label);
    let foundToken = findToken(document.parent);
    if (foundToken && game.userId === userId && game.settings.get('csm-lancer-qol', 'enableConditionEffects')) {
        log(await updateEffect(label, foundToken, true));
    }
    log(change);
    log(`${game.users.find(x => x.id === userId).name}(${userId})`);
});

Hooks.on('deleteActiveEffect', async function (document, change, userId) {
    log('**deleteActiveEffect**');
    const label = game.version < 10 ?
        document.data.label :
        document.label;
    log(label);
    let foundToken = findToken(document.parent);
    if (foundToken && game.userId === userId && game.settings.get('csm-lancer-qol', 'enableConditionEffects')) {
        log(await updateEffect(label, foundToken, false));
    }
    log(change);
    log(`${game.users.find(x => x.id === userId).name}(${userId})`);
});

Hooks.on('targetToken', function (user, targetedToken, isTargeted) {
    if (isTargeted) { // Only if we're targetting, not untargetting
        log(`${user.name} targeted ${targetedToken.name}`);
        if (game.settings.get('csm-lancer-qol', 'reactionReminder') !== 'd') { // If reaction reminder is enabled
            let targetedActor = game.version < 10 ?
                game.actors.find(x => x.id === targetedToken.data.actorId) :
                game.actors.find(x => x.id === targetedToken.actor.id);
            let ownership = game.version < 10 ?
                targetedActor.data.permission[game.userId] :
                targetedActor.ownership[game.userId];
            if (ownership == 3 && game.userId !== user.id) { // If you are the owner, and you did not perform the target ...,
                displayReactions(targetedActor, targetedToken);
            }
        }
    } else {
        log(`${user.name} untargeted ${targetedToken.name}`);
    }
});

async function handleSocketEvent({ action, payload }) {
    switch (action) {
        case "preLoadImageForAll": {
            await preLoadImageForAll(payload);
        }
    }
};

Hooks.on('updateCombat', combatTracking);

Hooks.on('createToken', preWreck);

Hooks.on("renderTileHUD", tileHUDButton);

Hooks.on('canvasReady', canvasReadyWreck);

Hooks.on('ready', function () {
    console.log('csm-lancer-qol | This code runs once core initialization is ready and' +
        ' game data is available.');
    registerSettings();
    game.modules.get('csm-lancer-qol').exposed = {
        setTimedEffect
    };
    if (game.version < 10) {
        socket.on('module.csm-lancer-qol', handleSocketEvent);
    } else {
        game.socket.on('module.csm-lancer-qol', handleSocketEvent);
    }
});

Hooks.on('setup', function () {
    console.log('csm-lancer-qol | Setup');
    addStatusEffects();
});
