import { log } from "./log.js";

function setFlaggedEffect(targetID, effect, duration, note, originID) {
    const statusEffect = CONFIG.statusEffects.find(x => x.label === effect);
    const target = canvas.tokens.placeables.find(x => x.id === targetID);
    let effectData = {
        label: statusEffect.label,
        icon: statusEffect.icon,
        flags: {
            'core': {
                statusEffectsEnabled: true,
                statusId: statusEffect.id
            },
            'csm-lancer-qol': {
                targetID: targetID,
                effect: statusEffect.label,
                duration: duration,
                note: note,
                originID: originID,
                appliedRound: game.combat.round
            }
        },
        changes: []
    };
    target.actor.createEmbeddedDocuments("ActiveEffect", [effectData]);
}

export function setTimedEffect(token) {
    if (!token) return ui.notifications.error(`Token not found!`);
    if (!game.combat) return ui.notifications.error('You are not in combat!');

    let durations = [
        {
            label: 'end of targets next turn',
            turns: 1,
            rounds: 1
        },
        {
            label: 'start of targets next turn',
            turns: 1,
            rounds: 1
        },
        {
            label: 'end of targets current turn',
            turns: 1,
            rounds: 0
        },
        {
            label: 'end of originators next turn',
            turns: 1,
            rounds: 1
        },
        {
            label: 'start of originators next turn',
            turns: 1,
            rounds: 1
        },
        {
            label: 'end of originators current turn',
            turns: 1,
            rounds: 0
        }
    ]

    new Dialog({
        title: "Set a Timed Effect",
        content: `
      <form>
        <div class="form-group">
          <label>Target:</label>
          <select id="targetID" name="targetID">
            ${canvas.tokens.placeables.map(n => `<option selected="${token.id}" value="${n.id}">${n.name}</option>`)}
          </select>
        </div>
        <div class="form-group">
          <label>Condition:</label>
          <select id="effect" name="effect">
            ${CONFIG.statusEffects.map(o => `<option value="${o.label}">${o.label}</option>`)}
          </select>
        </div>
        <div class="form-group">
          <label>Duration:</label>
          <select id="duration" name="duration">
            ${durations.map(p => `<option value="${p.label}">${p.label}</option>`)}
          </select>
        </div>
        <div class="form-group">
          <label>Note:</label>
          <input id="note" name="note" type="text" value="">
        </div>
        <div class="form-group">
          <label>Originator:</label>
          <select id="originID" name="originID">
            ${canvas.tokens.placeables.map(n => `<option selected="${token.id}" value="${n.id}">${n.name}</option>`)}
          </select>
        </div>
      </form>
    `,
        buttons: {
            ok: {
                label: "OK",
                callback: async (html) => {
                    let targetID = html.find('[name=targetID')[0].value;
                    let effect = html.find('[name=effect]')[0].value;
                    let duration = durations.find(x => x.label === html.find('[name=duration')[0].value);
                    let note = html.find('[name=note]')[0].value;
                    let originID = html.find('[name=originID')[0].value;
                    log(`TargetID selected: ${targetID}`);
                    log(`Effect selected: ${effect}`);
                    log(`Duration selected: ${duration.label} Turns: ${duration.turns} Rounds: ${duration.rounds}`);
                    log(`Note entered: ${note}`);
                    log(`OriginID selected: ${originID}`);
                    setFlaggedEffect(targetID, effect, duration, note, originID);
                }
            },
            cancel: {
                label: "Cancel"
            }
        }
    }).render(true);
}

function effectsReport(token) {
    log('**effectsReport**');
    const effects = token.actor.effects.filter(x => x.disabled === false);
    console.log(effects);
    if (effects.length > 0) {
        let html = `<h3>Effects on ${token.name}</h3>`;
        html += '<ul>';
        for (let m = 0; m < effects.length; m++) {
            let note = effects[m].getFlag('csm-lancer-qol', 'note');
            if (note) {
                html += `<li>${effects[m].label} - ${note}`;
            } else {
                html += `<li>${effects[m].label}`;
            }
        }
        html += '</ul>';
        ChatMessage.create({
            user: game.userId,
            content: html
        });
    }
}

function effectsReportv9(token) {
    log('**effectsReportv9**');
    let effects = [];
    if (token.data.actorLink) { // If linked, effects are on the actor
        let actor = game.actors.find(x => x.id === token.data.actorId);
        effects = actor.data.effects.filter(x => x.data.disabled === false);
        log(effects);
        if (effects.length > 0) {
            let html = `<h3>Effects on ${token.name}</h3>`;
            html += '<ul>';
            for (let m = 0; m < effects.length; m++) {
                let note = effects[m].data.flags['csm-lancer-qol']?.note || false;
                if (note) {
                    html += `<li>${effects[m].data.label} - ${note}`;
                } else {
                    html += `<li>${effects[m].data.label}`;
                }
            }
            html += '</ul>';
            ChatMessage.create({
                user: game.userId,
                content: html
            });
        }
    } else { // Not linked, effects are on the token in the actorData
        effects = token.data.actorData.effects.filter(x => x.disabled === false);
        log(effects);
        if (effects.length > 0) {
            let html = `<h3>Effects on ${token.name}</h3>`;
            html += '<ul>';
            for (let m = 0; m < effects.length; m++) {
                let note = effects[m].flags['csm-lancer-qol']?.note || false;
                if (note) {
                    html += `<li>${effects[m].label} - ${note}`;
                } else {
                    html += `<li>${effects[m].label}`;
                }
            }
            html += '</ul>';
            ChatMessage.create({
                user: game.userId,
                content: html
            });
        }
    }
}

// Start of a Turn
// 1. Decrement local "start" effects turn counter
// 2. Decrement remote "start" effects turn counter
// 3. Remove local "start" effects that are r0t0
// 4. Remove remote "start" effects that are r0t0
async function startOfTurn(token) {
    log('**endOfTurn**');
    log(token);
    let effects = token.actor.getEmbeddedCollection("ActiveEffect");
    for (let m = 0; m < effects.contents.length; m++) {
        let effect = effects.contents[m];
        if (typeof effect.getFlag('csm-lancer-qol', 'duration') !== 'undefined') {
            if (effect.getFlag('csm-lancer-qol', 'duration.label').includes('start')) {
                let turns = effect.getFlag('csm-lancer-qol', 'duration.turns');
                log(`${effect.label} - ${turns}`);
                if (turns > 0) {
                    turns--;
                    await effect.setFlag('csm-lancer-qol', 'duration.turns', turns);
                } else {
                    log(`${effect.label} is at ${turns} turns already.`);
                }
            } else {
                log(`${effect.data.label} does not end at the start of a turn.`);
            }
        } else {
            log(`${effect.data.label} is not a timed effect.`);
        }
    }
    let removeIds = token.actor.effects.filter(x =>
        x.getFlag('csm-lancer-qol', 'duration.turns') <= 0 &&
        x.getFlag('csm-lancer-qol', 'duration.rounds') <= 0 &&
        x.getFlag('csm-lancer-qol', 'duration.label').includes('start')
    );
    for (let n = 0; n < removeIds.length; n++) {
        await token.actor.deleteEmbeddedDocuments("ActiveEffect", [removeIds[n]._id]);
    }
}

async function startOfTurnv9(token) {
    log('**startOfTurnv9**');
    log(token);
    let effects = token.actor.getEmbeddedCollection("ActiveEffect");
    for (let m = 0; m < effects.contents.length; m++) {
        let effect = effects.contents[m];
        if (typeof effect.getFlag('csm-lancer-qol', 'duration') !== 'undefined') {
            if (effect.getFlag('csm-lancer-qol', 'duration.label').includes('start')) {
                let turns = effect.getFlag('csm-lancer-qol', 'duration.turns');
                if (turns > 0) {
                    turns--;
                    await effect.setFlag('csm-lancer-qol', 'duration.turns', turns);
                } else {
                    log(`${effect.data.label} is at ${turns} turns already.`);
                }
            } else {
                log(`${effect.data.label} does not end at the end of a turn.`);
            }
        } else {
            log(`${effect.data.label} is not a timed effect.`);
        }
    }
    let removeIds = token.actor.effects.filter(x =>
        x.getFlag('csm-lancer-qol', 'duration.turns') <= 0 &&
        x.getFlag('csm-lancer-qol', 'duration.rounds') <= 0 &&
        x.getFlag('csm-lancer-qol', 'duration.label').includes('start')
    );
    for (let n = 0; n < removeIds.length; n++) {
        await token.actor.deleteEmbeddedDocuments("ActiveEffect", [removeIds[n].data._id]);
    }
}

// End of a Turn
// 1. Decrement local "end" effects turn counter
// 2. Decrement remote "end" effects turn counter
// 3. Remove local "end" effects that are r0t0
// 4. Remove remote "end" effects that are r0t0
async function endOfTurn(token) {
    log('**endOfTurn**');
    log(token);
    let effects = token.actor.getEmbeddedCollection("ActiveEffect");
    for (let m = 0; m < effects.contents.length; m++) {
        let effect = effects.contents[m];
        if (typeof effect.getFlag('csm-lancer-qol', 'duration') !== 'undefined') {
            if (effect.getFlag('csm-lancer-qol', 'duration.label').includes('end')) {
                let turns = effect.getFlag('csm-lancer-qol', 'duration.turns');
                if (turns > 0) {
                    turns--;
                    await effect.setFlag('csm-lancer-qol', 'duration.turns', turns);
                } else {
                    log(`${effect.label} is at ${turns} turns already.`);
                }
            } else {
                log(`${effect.data.label} does not end at the end of a turn.`);
            }
        } else {
            log(`${effect.data.label} is not a timed effect.`);
        }
    }
    let removeIds = token.actor.effects.filter(x =>
        x.getFlag('csm-lancer-qol', 'duration.turns') <= 0 &&
        x.getFlag('csm-lancer-qol', 'duration.rounds') <= 0 &&
        x.getFlag('csm-lancer-qol', 'duration.label').includes('end')
    );
    for (let n = 0; n < removeIds.length; n++) {
        await token.actor.deleteEmbeddedDocuments("ActiveEffect", [removeIds[n]._id]);
    }
}

async function endOfTurnv9(token) {
    log('**endOfTurnv9**');
    log(token);
    let effects = token.actor.getEmbeddedCollection("ActiveEffect");
    for (let m = 0; m < effects.contents.length; m++) {
        let effect = effects.contents[m];
        if (typeof effect.getFlag('csm-lancer-qol', 'duration') !== 'undefined') {
            if (effect.getFlag('csm-lancer-qol', 'duration.label').includes('end')) {
                let turns = effect.getFlag('csm-lancer-qol', 'duration.turns');
                if (turns > 0) {
                    turns--;
                    await effect.setFlag('csm-lancer-qol', 'duration.turns', turns);
                } else {
                    log(`${effect.data.label} is at ${turns} turns already.`);
                }
            } else {
                log(`${effect.data.label} does not end at the end of a turn.`);
            }
        } else {
            log(`${effect.data.label} is not a timed effect.`);
        }
    }
    let removeIds = token.actor.effects.filter(x =>
        x.getFlag('csm-lancer-qol', 'duration.turns') <= 0 &&
        x.getFlag('csm-lancer-qol', 'duration.rounds') <= 0 &&
        x.getFlag('csm-lancer-qol', 'duration.label').includes('end')
    );
    for (let n = 0; n < removeIds.length; n++) {
        await token.actor.deleteEmbeddedDocuments("ActiveEffect", [removeIds[n].data._id]);
    }
}

// End of a Round
// 1. Decrement all effect round counters by 1
async function endOfRound(combat) {
    let tokens = [];
    for (let i = 0; i < combat.combatants.contents.length; i++) {
        tokens.push(combat.combatants.contents[i].tokenId);
    }
    for (let j = 0; j < tokens.length; j++) {
        let token = canvas.tokens.placeables.find(x => x.id === tokens[j]);
        let effects = token.actor.getEmbeddedCollection("ActiveEffect");
        for (let m = 0; m < effects.contents.length; m++) {
            let effect = effects.contents[m];
            if (typeof effect.getFlag('csm-lancer-qol', 'duration') !== 'undefined') {
                let rounds = effect.getFlag('csm-lancer-qol', 'duration.rounds');
                if (rounds > 0) {
                    rounds--;
                    await effect.setFlag('csm-lancer-qol', 'duration.rounds', rounds);
                } else {
                    log(`${effect.label} is at ${rounds} rounds already.`);
                }
            } else {
                log(`${effect.data.label} is not a timed effect.`);
            }
        }
    }
    return tokens;
}

async function endOfRoundv9(combat) {
    let tokens = [];
    for (let i = 0; i < combat.combatants.contents.length; i++) {
        tokens.push(combat.combatants.contents[i].data.tokenId);
    }
    for (let j = 0; j < tokens.length; j++) {
        let token = canvas.tokens.placeables.find(x => x.id === tokens[j]);
        let effects = token.actor.getEmbeddedCollection("ActiveEffect");
        for (let m = 0; m < effects.contents.length; m++) {
            let effect = effects.contents[m];
            if (typeof effect.getFlag('csm-lancer-qol', 'duration') !== 'undefined') {
                let rounds = effect.getFlag('csm-lancer-qol', 'duration.rounds');
                if (rounds > 0) {
                    rounds--;
                    await effect.setFlag('csm-lancer-qol', 'duration.rounds', rounds);
                } else {
                    log(`${effect.data.label} is at ${rounds} rounds already.`);
                }
            } else {
                log(`${effect.data.label} is not a timed effect.`);
            }
        }
    }
    return tokens;
}

export async function combatTracking(combat, changed, options, user) {
    if (game.settings.get('csm-lancer-qol', 'effectsTimer')) {
        log('**updateCombat**');
        // log(combat);
        if (combat.current.combatantId !== null) { // Turn start
            let currentCombatant = game.version < 10 ?
                combat.combatants.find(x => x.data._id === combat.current.combatantId) :
                combat.combatants.find(x => x._id === combat.current.combatantId);
            let currentActor = game.version < 10 ?
                game.actors.find(x => x.id === currentCombatant.data.actorId) :
                game.actors.find(x => x.id === currentCombatant.actorId);
            let currentToken = game.version < 10 ?
                canvas.tokens.placeables.find(x => x.id === currentCombatant.data.tokenId) :
                canvas.tokens.placeables.find(x => x.id === currentCombatant.tokenId);
            log(`Current Turn Actor: ${currentActor.name}`);
            log(`Current Turn Token: ${currentToken.name}`);
            if (game.user.isGM) {
                if (game.version < 10) {
                    startOfTurnv9(currentToken);
                    effectsReportv9(currentToken);
                } else {
                    startOfTurn(currentToken);
                    effectsReport(currentToken);
                }
            }
        }
        if (combat.previous.combatantId !== null) { // Turn end
            let previousCombatant = game.version < 10 ?
                combat.combatants.find(x => x.data._id === combat.previous.combatantId) :
                combat.combatants.find(x => x._id === combat.previous.combatantId);
            let previousActor = game.version < 10 ?
                game.actors.find(x => x.id === previousCombatant.data.actorId) :
                game.actors.find(x => x.id === previousCombatant.actorId);
            let previousToken = game.version < 10 ?
                canvas.tokens.placeables.find(x => x.id === previousCombatant.data.tokenId) :
                canvas.tokens.placeables.find(x => x.id === previousCombatant.tokenId);
            log(`Previous Turn Actor: ${previousActor.name}`);
            log(`Previous Turn Token: ${previousToken.name}`);
            if (game.user.isGM) {
                if (game.version < 10) {
                    endOfTurnv9(previousToken);
                } else {
                    endOfTurn(previousToken);
                }
            }
        }
        if (changed.round) {
            log(`ROUND ${changed.round}! FIGHT!`);
            if (game.user.isGM) {
                if (game.version < 10) {
                    log(await endOfRoundv9(combat));
                } else {
                    log(await endOfRound(combat));
                }
            }
        }
        // log(changed);
        // log(options);
        // log(user);
    }
}
