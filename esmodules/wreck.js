import { log } from "./log.js";

// Is a token biological so we don't wreck biological tokens
export function isBiological(token) {
    const actor = game.version < 10 ?
        game.actors.find(x => x.id === token.data.actorId) :
        game.actors.find(x => x.id === token.document.actorId);
    const biologicalRoleItems = game.version < 10 ?
        actor.items.filter(x => x.data.data.role === 'biological') :
        actor.items.filter(x => x.system.role === 'biological');
    if (biologicalRoleItems.length > 0) {
        log(`${token.name} is biological.`)
        return true;
    } else {
        log(`${token.name} is not biological.`)
        return false;
    }
}

// Load a texture and signal to other clients to load it as well is push is true
export async function preLoadImageForAll(src, push = false) {
    if (push) {
        if (game.version < 10) {
            socket.emit('module.csm-lancer-qol', { action: "preLoadImageForAll", payload: src });
        } else {
            game.socket.emit('module.csm-lancer-qol', { action: "preLoadImageForAll", payload: src });
        }
    }
    await loadTexture(src);
    return src;
}

// Find a random wreck image from one or both image locations that is the right size
async function getWreckImage(size) {
    let wreckList = [];
    if (size < 1) { size = 1; } // We don't support smaller sizes right now
    if (size > 3) { size = 3; } // We don't support larger sizes right now

    const dataWrecksPath = 'modules/csm-lancer-qol/wrecks/s' + size;
    const dataWreckImages = await FilePicker.browse('data', dataWrecksPath);
    for (const path of dataWreckImages.files) { // Add images from data to the array
        wreckList.push(path);
    }

    if (game.settings.get('csm-lancer-qol', 'userWrecksPath') != '') {
        const userWrecksPath = game.settings.get('csm-lancer-qol', 'userWrecksPath') + '/s' + size;
        const userWreckImages = await FilePicker.browse('user', userWrecksPath);
        for (const path of userWreckImages.files) { // Add images from user to the array
            wreckList.push(path);
        }
    }

    log(wreckList);
    const rand = Math.floor(Math.random() * (wreckList.length));
    log(rand);
    return wreckList[rand];
}

// If a token is not "dead", remove effects, replace the image with a wreck, and clear heat/burn/overshield
export async function wreckIt(token) {
    const isDead = token.document.getFlag('csm-lancer-qol', 'isDead');
    if (isDead) {
        log(`${token.name} is already wrecked.`);
    } else {
        await TokenMagic.deleteFilters(token);
        const size = game.version < 10 ?
            token.actor.data.data.derived.mm.Size :
            token.actor.system.derived.mm.Size;
        const imgString = token.document.getFlag('csm-lancer-qol', 'wreckImgPath');
        log(`Picked ${imgString} for ${token.name}`);
        const wreckType = game.settings.get('csm-lancer-qol', 'wreckType');
        let tileWreck = false
        switch(wreckType){
            case 'token':
                break;
            case 'PCtoken':
                tileWreck = !(token.actor.type == "mech");
                break;
            case "linkToken":
                tileWreck = !token.document.actorLink;
                break;
            case 'tile':
                tileWreck = true
                break;
        }
        if(tileWreck){
            //Need to take into account the non-square measurements of hex tokens
            //Testing shows maybe not? Or at least that assuming square gives better results on large tokens.
            //const hexConstant = 1.155;
            //const heightMultiplier = canvas.scene.grid.type == CONST.GRID_TYPES.HEXODDR || canvas.scene.grid.type == CONST.GRID_TYPES.HEXEVENR ? hexConstant : 1;
            //const widthMultiplier = canvas.scene.grid.type == CONST.GRID_TYPES.HEXODDQ || canvas.scene.grid.type == CONST.GRID_TYPES.HEXEVENQ ? hexConstant : 1;
            const gridSize = canvas.scene.grid.size;

            const tileData = {
                x: token.document.x,
                y: token.document.y,
                height: token.document.height * gridSize/* * heightMultiplier*/,
                width: token.document.width * gridSize /* * widthMultiplier*/,
                flags: {
                    'csm-lancer-qol': {
                        isWreck: true,
                        tokenDocument: token.document.toObject() //so we can restore the token later, once I've written that part
                    }
                }
            }
            game.version < 10 ? tileData.img = imgString : tileData.texture = {src: imgString};
            await canvas.scene.createEmbeddedDocuments("Tile", [tileData])
            await token.document.delete()
            return; //return something falsy to prevent a later update - returning the token doesn't work here for some reason as it stays defined.

        } else { //it's a token wreck, proceed as before
            const updates = game.version < 10 ?
                [{
                    _id: token.id,
                    img: imgString,
                    flags: {
                        'csm-lancer-qol': {
                            isDead: true,
                            originalImgPath: token.data.img
                        }
                    }
                }]
                :
                [{
                    _id: token.document._id,
                    texture: {
                        src: imgString
                    },
                    flags: {
                        'csm-lancer-qol': {
                            isDead: true,
                            originalImgPath: token.document.texture.src
                        }
                    }
                }];
            await canvas.scene.updateEmbeddedDocuments("Token", updates);
        } 
    }
    return token;
}

// If a token is dead, return it's image to a non-wreck original image
export async function unWreckIt(token) {
    const isDead = token.document.getFlag('csm-lancer-qol', 'isDead');
    if (isDead) {
        log(`${token.name} is back from the dead!`);
        const originalImgPath = token.document.getFlag('csm-lancer-qol', 'originalImgPath');

        const updates = game.version < 10 ?
            [{
                _id: token.id,
                img: originalImgPath,
                flags: {
                    'csm-lancer-qol': {
                        isDead: false
                    }
                }
            }]
            :
            [{
                _id: token.document._id,
                texture: {
                    src: originalImgPath
                },
                flags: {
                    'csm-lancer-qol': {
                        isDead: false
                    }
                }
            }];
        await canvas.scene.updateEmbeddedDocuments("Token", updates);
    } else {
        log(`${token.name} is already unwrecked.`);
    }
    return token;
}

export async function unWreckTile(tile){
    const isWreck = tile.getFlag('csm-lancer-qol', "isWreck");
    if(isWreck){
        let tokenData = tile.getFlag('csm-lancer-qol', "tokenDocument");
        const actor = game.actors.get(tokenData.actorId);
        if(!actor){
            //Can't do anything if the source actor has been deleted
            log(`No actor found for token wreck ${tokenData.name}`);
            return
        }
        log(`${tokenData.name} is back from the dead!`);
        //set the position to the position of the wreck, in case it was moved
        tokenData.x = tile.x;
        tokenData.y = tile.y
        
        if(tokenData.actorLink){
            //ensure actor has at least 1 struct and 1 hp
            if(actor.system.structure == 0){
                actor.update({"system.structure": 1, "system.hp": 1} )
            }
        } else {
            if(game.version < 10){
                tokenData.actorData.data.hp = 1
                tokenData.actorData.data.structure = 1
            } else {
                tokenData.actorData.system.hp = 1
                tokenData.actorData.system.structure = 1
            }
        }
        await tile.delete()
        return await canvas.scene.createEmbeddedDocuments("Token", [tokenData])
    } else {
        log(`Tile ID ${tile.id} is not a wreck`)
    }
}

// When we drag a token onto a scene, pick a wreck texture, ask everyone to load it, and save it to a flag
export async function preWreck(document, change, userId) {
    if (game.user.isGM) {
        const size = game.version < 10 ?
            document.data.flags.lancer.mm_size :
            document.flags.lancer.mm_size;
        const wreckImgPath = await getWreckImage(size);
        await preLoadImageForAll(wreckImgPath, true);
        await document.setFlag('csm-lancer-qol', 'wreckImgPath', wreckImgPath);
        log(document);
    }
    if (userId) { // Sometimes this is called without a userId
        log(`${game.users.find(x => x.id === userId).name}(${userId})`);
    }
}

// When a scene loads, let's pre-load all the wreck textures
// If a token doesn't have one, let's add one, for fun
export async function canvasReadyWreck() {
    for (const token of canvas.tokens.placeables) {
        const wreckImgPath = game.version < 10 ?
            await token.data.flags['csm-lancer-qol']?.wreckImgPath :
            await token.document.flags['csm-lancer-qol']?.wreckImgPath;
        if (typeof wreckImgPath === 'undefined') {
            console.log(`csm-lancer-qol | ${token.name} has no wreckImgPath ...`);
            await preWreck(token.document);
        } else {
            await preLoadImageForAll(wreckImgPath);
            console.log(`csm-lancer-qol | Loaded ${wreckImgPath} for ${token.name}`);
        }
    }
}