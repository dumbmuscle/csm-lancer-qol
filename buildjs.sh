#!/bin/env bash

# Read the macro.db and generate human readable js files

while IFS="" read -r p || [ -n "$p" ]; do
    filename=$( printf '%s\n' "$p" | jq -r .name )
    filename=$( echo "$filename" | tr '[:upper:]' '[:lower:]' )
    filename=$( echo "$filename" | sed -e 's/[^A-Za-z0-9._-]/_/g' )
    command=$( printf '%s\n' "$p" | jq -r .command )
    echo $filename.js
    echo "$command" > "src/scripts/$filename.js"
done <packs/macro.db
