if (!token) return ui.notifications.error(`Token not found!`);

let effects = token.actor.getEmbeddedCollection("ActiveEffect");
let timedEffects = effects.filter(x => typeof x.getFlag('csm-lancer-qol', 'duration') !== 'undefined');
for (i = 0; i < timedEffects.length; i++) {
    let effect = timedEffects[i];
    console.log(effect);
    console.log(effect.data.label);
    console.log(`Label: ${effect.getFlag('csm-lancer-qol', 'duration.label')}`);
    console.log(`Turns: ${effect.getFlag('csm-lancer-qol', 'duration.turns')}`);
    console.log(`Rounds: ${effect.getFlag('csm-lancer-qol', 'duration.rounds')}`);
}