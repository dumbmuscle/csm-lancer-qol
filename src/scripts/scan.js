let targets = Array.from(game.user.targets);

targets.forEach(target => {
    let sc_dir = target.document.actor._prev_derived.mm
    let hase_table_html = `
    <table>
    <tr>
    <th>HULL</th><th>AGI</th><th>SYS</th><th>ENG</th>
    </tr>
    <tr>
    <td>${sc_dir.Hull}</td><td>${sc_dir.Agi}</td><td>${sc_dir.Sys}</td><td>${sc_dir.Eng}</td>
    </tr>
    </table>
    `
    let stat_table_html = `
    <table>
    <tr>
    <th>Armor</th><th>HP</th><th>Heat</th><th>Speed</th>
    </tr>
    <tr>
    <td>${sc_dir.Armor}</td><td>${sc_dir.CurrentHP}/${sc_dir.MaxHP}</td><td>${sc_dir.CurrentHeat}/${sc_dir.HeatCapacity}</td><td>${sc_dir.Speed}</td>
    </tr>
    <tr>
    <th>Evasion</th><th>E-Defense</th><th>Save</th><th>Sensors</th>
    </tr>
    <tr>
    <td>${sc_dir.Evasion}</td><td>${sc_dir.EDefense}</td><td>${sc_dir.SaveTarget}</td><td>${sc_dir.SensorRange}</td>
    </tr>
    <td>
    </table>
    `
    let sc_list = ``
    if (!sc_dir._features || sc_dir._features.length == 0) {
        sc_list += "<p>NONE</p>";
    } else {
        sc_dir._features.forEach(i => {
            let sc_desc = ``
            if (i.Effect) {
                sc_desc = i.Effect
            } else {
                sc_desc = "No description given."
            }
            if (i.Trigger) {
                sc_desc = "Trigger: " + i.Trigger + "<br>" + sc_desc
            }
            let sc_entry = "<details><summary>" + i.Name + "</summary><p>" + sc_desc + "</p></details>"
            sc_list += sc_entry
        });
    }


    let sc_templates = ``
    if (!sc_dir._templates || sc_dir._templates.length == 0) {
        sc_templates += "<p>NONE</p>";
    } else {
        sc_dir._templates.forEach(i => {
            let sc_entry = "<p>" + i.Name + "</p>"
            sc_templates += sc_entry
        });
    }

    sc_templates += "<br>"

    ChatMessage.create({
        user: game.user._id,
        content: `<h2>Scan results: ${sc_dir.Name}</h2>` + hase_table_html + stat_table_html + `<h3>Templates:</h3>` + sc_templates + `<h3>Systems:</h3>` + sc_list
    });
})
