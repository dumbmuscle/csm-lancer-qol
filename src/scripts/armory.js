async function updateAmmoCases(mech) {
    const cases = ['none', 'Ammo Case I', 'Ammo Case II', 'Ammo Case III'];
    let pilot = game.version < 10 ?
        game.actors.find(x => x.id === mech.data.data.pilot.id) :
        game.actors.find(x => x.id === mech.system.pilot.id);
    let armoryRank = game.version < 10 ?
        pilot.data.items.find(x => x.data.name === 'WALKING ARMORY')?.data.data.curr_rank :
        pilot.items.find(x => x.name === 'WALKING ARMORY')?.system.curr_rank;
    let sheet_data = await mech.sheet.getDataLazy();
    console.log(`${pilot.name} is the pilot of ${mech.name} and has a Walking Armory rank of ${armoryRank}`);

    if (typeof armoryRank === 'undefined') {
        console.log(`Removing all Ammo Cases from ${mech.name}.`);
        for (i = 1; i < cases.length; i++) {
            sheet_data.mm.Loadout.Systems.find(x => x.Name === cases[i])?.destroy_entry();
        }
    } else {
        console.log(`Make sure ${mech.name} has an Ammo Case.`);
        console.log(sheet_data.mm.Loadout.Systems.filter(x => x.Name === cases[armoryRank]).length);
        if (sheet_data.mm.Loadout.Systems.filter(x => x.Name === cases[armoryRank]).length == 0) {
            const mechSystemPack = await game.packs.get("world.mech_system");
            let installSystemId = mechSystemPack.index.find(x => x.name === cases[armoryRank]);
            let installSystemObject = await mechSystemPack.getDocument(installSystemId._id);
            let installSystemObjectMm = game.version < 10 ?
                installSystemObject.data.data.derived.mm :
                installSystemObject.system.derived.mm;
            await mech.sheet.on_root_drop(installSystemObjectMm);
        }
    }
}

for (const actor of game.actors) {
    let type = game.version < 10 ?
        actor.data.type :
        actor.type;
    if (type === 'mech') { await updateAmmoCases(actor); }
}