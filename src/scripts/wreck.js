async function getWreckImage(size) {
    let wreckList = [];
    if (size < 1) { size = 1; } // We don't support smaller sizes right now

    const dataWrecksPath = 'modules/csm-lancer-qol/wrecks/s' + size;
    const dataWreckImages = await FilePicker.browse('data', dataWrecksPath);
    for (const path of dataWreckImages.files) { // Add images from data to the array
        wreckList.push(path);
    }

    if (game.settings.get('csm-lancer-qol', 'userWrecksPath') != '') {
        const userWrecksPath = game.settings.get('csm-lancer-qol', 'userWrecksPath') + '/s' + size;
        const userWreckImages = await FilePicker.browse('user', userWrecksPath);
        for (const path of userWreckImages.files) { // Add images from user to the array
            wreckList.push(path);
        }
    }

    console.log(wreckList);
    const rand = Math.floor(Math.random() * (wreckList.length));
    console.log(rand);
    return wreckList[rand];
}

async function wreckItv9(token) {
    const isDead = token.document.getFlag('csm-lancer-qol', 'isDead');
    if (isDead) {
        console.log(`${token.name} is already wrecked.`);
    } else {
        await TokenMagic.deleteFilters(token);
        const size = token.actor.data.data.derived.mm.Size;
        const imgString = await getWreckImage(size);
        console.log(`Picked ${imgString} for ${token.name}`);

        const updates = [{
            _id: token.id,
            img: imgString,
            flags: {
                'csm-lancer-qol': {
                    isDead: true,
                    originalImgPath: token.data.img
                }
            }
        }];
        await canvas.scene.updateEmbeddedDocuments("Token", updates);
    }
    return token;
}

async function wreckIt(token) {
    const isDead = token.document.getFlag('csm-lancer-qol', 'isDead');
    if (isDead) {
        console.log(`${token.name} is already wrecked.`);
    } else {
        await TokenMagic.deleteFilters(token);
        const size = token.document.width;
        const imgString = await getWreckImage(size);
        console.log(`Picked ${imgString} for ${token.name}`);

        const updates = [{
            _id: token.document._id,
            texture: {
                src: imgString
            },
            flags: {
                'csm-lancer-qol': {
                    isDead: true,
                    originalImgPath: token.document.texture.src
                }
            }
        }];
        await canvas.scene.updateEmbeddedDocuments("Token", updates);
    }
    return token;
}

let i = 0;
target = null;
while (i < arguments.length) {
    if (arguments[i] != undefined) {
        if (arguments[i].constructor.name == "LancerToken") {
            target = arguments[i];
            if (game.version < 10) {
                await wreckItv9(target);
            } else {
                await wreckIt(target);
            }
        } else if (arguments[i].constructor.name == "LancerTokenDocument") {
            target = arguments[i]._object;
            if (game.version < 10) {
                await wreckItv9(target);
            } else {
                await wreckIt(target);
            }
        }
    }
    i++;
}
