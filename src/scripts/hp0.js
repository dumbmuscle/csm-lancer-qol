if (canvas.tokens.controlled.length !== 1) {
	ui.notifications.error("Select one and only one token.");
	return;
}

let selectedActor = game.actors.get(canvas.tokens.controlled[0].actor.id)
//console.log(selectedActor);

async function scratchedThePaint() {
	await selectedActor.update({ data: { hp: 1 } });
	await selectedActor.updateEmbeddedDocuments("Item", [{ _id: CPJId, 'data.destroyed': true }])

	let pilotObject
	let mechPilotObject = await selectedActor.data.data.pilot
	if (mechPilotObject === null) {
		pilotObject = await selectedActor
		//console.log(pilotObject)
	} else {
		pilotObject = await game.actors.get(mechPilotObject.id)
		//console.log(pilotObject)
	}
	console.log(pilotObject)
	let myActor = pilotObject || game.user.character || canvas.tokens.controlled[0].actor; //Last resort fallbacks. It's unlikely that pilotObject would be invalid in a scenario without full HP automation coming into play.
	//console.log(myActor)
	let myActorName = await myActor.data.name
	ChatMessage.create({
		speaker: ChatMessage.getSpeaker({ actor: myActor }),
		content: "That only scratched my paint!" //Feel free to expand upon this if the single line is too generic.
	});

	return "Done.";
};

let itemMap = await selectedActor.data.items.contents;

//console.log(itemMap)
let CPJFound = false;
let CPJId = "";
let CPJIndex = 0;
for (let i = 0; i < itemMap.length; i++) {
	let moduleName = await itemMap[i].data.name;
	if (moduleName == "CUSTOM PAINT JOB") {
		if (itemMap[i].data.data.destroyed == true) {
			console.log("Custom Paintjob has already been used.");
		} else {
			console.log("Custom Paintjob found.");
			CPJId = await itemMap[i].data._id;
			CPJFound = true;
			CPJIndex = i;
			break;
		};
	};
};

if (CPJFound == true) {
	let rolled = await new Roll('1d6').roll({ async: true });
	let rollTotal = await rolled.total
	console.log(rollTotal)
	if (rollTotal == 6) {
		console.log(await scratchedThePaint())
		canvas.tokens.controlled[0].drawBars();
	} else {
		game.lancer.prepareStructureMacro(canvas.tokens.controlled[0].actor.id);
	}
} else {
	game.lancer.prepareStructureMacro(canvas.tokens.controlled[0].actor.id);
	canvas.tokens.controlled[0].drawBars();
};
